const gulp        = require('gulp');
const browserSync = require('browser-sync').create();

gulp.task('serve', function() {

    browserSync.init({
        server: "./"
    });

    gulp.watch(["css/*.css","*.html"]).on('change', browserSync.reload);
});